

get_clickhouse_res <- function(query){
  con <- DBI::dbConnect(RClickhouse::clickhouse(), password = config::get("pass"))
  res <- DBI::dbGetQuery(con, query) 
  dbDisconnect(con)
  return(res)
}


parse.module.name <- function(x) {
  r <- x
  r[grepl(":", x, fixed = TRUE)] <- "others(online,recent browse, ..)"
  pattern <- "hotitem"
  r[grepl(pattern, x, fixed = TRUE) & (!grepl(":", x, fixed = TRUE))] <- pattern
  for(pattern in c("_offline_", "offline_nn", "online_nn")) {
    r[grepl(pattern, x, fixed = TRUE)] <- pattern
  }
  for(pattern in c("_online_tnn", "_online_rnn", "_online_ensemble_nn", "_online_nn-decay", "_offline_nn-decay")) {
    i <- grepl(pattern, x, fixed = TRUE) & (!grepl(":", x, fixed = TRUE))
    xx <- x[i]
    xx <- sub("[^_]+_", "", xx)
    r[i] <- xx
  }
  for(pattern in c("AB_A_offline_matrix", "AB_B_offline_hotitem")) {
    r[grepl(pattern, x, fixed = TRUE) & (!grepl(":", x, fixed = TRUE))] <- pattern
  }
  r[x == "def"] <- "def"
  r[x == "fix"] <- "fix"
  r[x == "fix_psid"] <- "fix_psid"
  r  
}


as.sunburstDF <- function(DF, valueCol = NULL){
  require(data.table)
  
  colNamesDF <- names(DF)
  
  if(is.data.table(DF)){
    DT <- copy(DF)
  } else {
    DT <- data.table(DF, stringsAsFactors = FALSE)
  }
  
  DT[, root := "Total"]
  colNamesDT <- names(DT)
  
  if(is.null(valueCol)){
    setcolorder(DT, c("root", colNamesDF))
  } else {
    setnames(DT, valueCol, "values", skip_absent=TRUE)
    setcolorder(DT, c("root", setdiff(colNamesDF, valueCol), "values"))
  }
  
  hierarchyCols <- setdiff(colNamesDT, "values")
  hierarchyList <- list()
  
  for(i in seq_along(hierarchyCols)){
    currentCols <- colNamesDT[1:i]
    if(is.null(valueCol)){
      currentDT <- unique(DT[, ..currentCols][, values := .N, by = currentCols], by = currentCols)
    } else {
      currentDT <- DT[, lapply(.SD, sum, na.rm = TRUE), by=currentCols, .SDcols = "values"]
    }
    setnames(currentDT, length(currentCols), "labels")
    hierarchyList[[i]] <- currentDT
  }
  
  hierarchyDT <- rbindlist(hierarchyList, use.names = TRUE, fill = TRUE)
  
  parentCols <- setdiff(names(hierarchyDT), c("labels", "values", valueCol))
  hierarchyDT[, parents := apply(.SD, 1, function(x){fifelse(all(is.na(x)), yes = NA_character_, no = paste(x[!is.na(x)], sep = ":", collapse = " - "))}), .SDcols = parentCols]
  hierarchyDT[, ids := apply(.SD, 1, function(x){paste(x[!is.na(x)], collapse = " - ")}), .SDcols = c("parents", "labels")]
  hierarchyDT[, c(parentCols) := NULL]
  return(hierarchyDT)
}
