# Improve ways

### If User offset days is mainly in 8-28 days group
1. enlarge redis
   2. long-term model 

### If User offset 0-7 days rate differs a lot from Optimal normal model rate 
1. check user behavior data is in redis 

### Improve item coverage (see Improve by enlarge model size page)
* enlarge model size to see how much coverage can be improve (100k, … , 500k)
  * enlarge model size and check the product that can’t be cover is come from new item or unpopular item

##### If lots of items come from new item
1. try to update datafeed more frequently or use the current datafeed 

##### If lots of items come from unpopular item
1. try to enlarge model size more

