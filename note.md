#### Note: 
##### 1. If sites in 'high no event rate', check with bidding team whether bids come from users without event.
##### 2. If sites in 'not in model', click 'improve by enlarge model size' tab to see the improvement.
##### 3. If sites in 'seems ok', but the no_event_rate still high, go to 'Main dashboard' tab to see the details.