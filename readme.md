This dashboard is written by R shiny, which represents the recommendation performance breakdown by date, oid, cid, offset_date, and datafeed stock status. 

The dashboard structure are mainly in `ui.R` and `server.R`, which include the code needed to define the UI and server components, respectively.

The scope is as the followings.

1. update order.json to get the site_id, oid, oid_name information
2. query ClickHouse by date, site, oid input
3. show table or plot outputs

How to run the app

1. copy config.yaml
2. run `Rscript setup.R`