server <- function(input, output, session) {
  tab_list <- NULL
  
  ##' observe functions to get the data
  ###' invalidate every 24 hrs to update order.json
  observe({
    invalidateLater(60 * 60 * 24 * 1000, session) 
    mysql <- DBI::dbConnect(
      RMySQL::MySQL(), 
      group="ai-campaign-config-db-prd"
    )
    order_info <- dplyr::tbl(mysql, "order_info")
    campaign <- dplyr::tbl(mysql, "campaign")
    
    
    order_json <- left_join(
      campaign %>%
        select(oid, cid, cid_name = name),
      order_info %>%
        select(oid, oid_name = name, vertical, retargeting_site_id),
      by = "oid"
    ) %>%
      collect() %>%
      as.data.table(key = "cid") %>% select(oid, oid_name, vertical) %>% unique() %>%
      set_colnames(c('oid', 'name', 'vertical'))
    
    
    site_oid <- get_clickhouse_res(sprintf (
      "select distinct site_id, oid from rec_performance.no_event_offset_alg_summary where show >0 ")) %>%
      left_join(order_json, by = 'oid')  %>%
      mutate(oid_info = paste(name, ';', vertical, ';', oid)) 
    print('update order.json')
    lapply(dbListConnections(MySQL()), dbDisconnect)
    
    
  })
  
  
  observeEvent(input$site_id, {
    updateSelectizeInput(session,
                         "oid_name_vertical",
                         choices = site_oid$oid_info[site_oid$site_id == input$site_id],
                         selected = site_oid$oid_info[site_oid$site_id == input$site_id][1],
                         server = TRUE)
  })
  
  
  performance_breakdown_all <- reactive({
    res <- get_clickhouse_res(sprintf (
      "select * from rec_performance.no_event_offset_alg_summary where bid_date = '%s'", input$date)) %>% 
      mutate(major_offset_date = as.numeric(major_offset_date),
             all_offset_date = as.numeric(all_offset_date)) %>%
      filter(show > 0 ) %>% 
      left_join(order_json, by = 'oid') 
    res
  })
  
  
  
  performance_breakdown <- reactive({
    res <- get_clickhouse_res(sprintf (
      "select * from rec_performance.no_event_offset_alg_summary where bid_date = '%s' and  site_id = '%s' ", input$date, input$site_id)) %>% 
      mutate(major_offset_date = as.numeric(major_offset_date),
             all_offset_date = as.numeric(all_offset_date)) %>%
      filter(show > 0 ) %>% 
      left_join(order_json, by = 'oid')  %>% 
      filter(oid == site_oid$oid[ site_oid$oid_info == input$oid_name_vertical]) %>%
      mutate(alg = parse.module.name(first_alg))
    
    res 
  })
  
  
  rt_coverage_data <- reactive({
    res <- get_clickhouse_res(
      query = sprintf("select * from rec_performance.datafeed_rt_coverage where  rt_date = '%s' and site_id = '%s'", 
                      input$date, input$site_id)) %>% 
      mutate(site_id = as.character(site_id), datafeed_id = as.character(datafeed_id), rt_date = as.character(rt_date)) 
    
    
    rt_coverage <- res  %>% group_by(site_id, datafeed_id, in_stock) %>%
      summarise(uniq_product = sum(uniq_product), visit = sum(visit)) %>%
      mutate(product_perc = round(uniq_product/sum(uniq_product),3), visit_perc = round(visit/sum(visit),2)) 
    
    rt_coverage
    
  })
  
  
  rt_coverage_trend <- reactive({
    res <- get_clickhouse_res(
      query = sprintf("select * from rec_performance.datafeed_rt_coverage where site_id = '%s'", 
                      input$site_id)) %>% 
      #group_by(rt_date) %>%
      #mutate(visit = sum(visit)) %>%
      mutate(site_id = as.character(site_id), datafeed_id = as.character(datafeed_id), rt_date = as.character(rt_date)) 
    #filter(rt_date == current_date ) %>%
    #filter(site_id == input$site_id)
    res
  })
  
  
  campaign_quality <- reactive({
    
    performance_breakdown_all() %>% 
      group_by(site_id, have_event) %>% 
      summarise(show = sum(show), click = sum(click), action = sum(action), cost = sum(cost), revenue = sum(revenue)) %>%
      mutate(atr = action/ show, cpm = (cost/ 1e7) / (show/1000)) %>%
      group_by(site_id) %>% 
      summarise(atr_ratio = atr[have_event == 'no_event']/ atr[have_event == 'have_event'],
                cost_ratio = cost[have_event == 'no_event']/ cost[have_event == 'have_event'],
                cpm_ratio = cpm[have_event == 'no_event']/ cpm[have_event == 'have_event'],
                total_show = sum(show), total_cost = sum(cost)/1e7) %>%
      arrange(desc(total_cost))
    
    
  })
  
  
  offset_info <- reactive({
    
    performance_breakdown()  %>% as.data.frame() %>%
      mutate(offset_date_grp = if_else( major_offset_date <= 7, '0-7', 
                                        if_else( major_offset_date > 7 & major_offset_date < 28, '8-28', 'others'))) %>%
      group_by(offset_date_grp) %>% 
      summarise(show = sum(show))  %>%
      mutate(perc_main_event = show/sum(show)) 
    
  })
  
  
  
  rt_coverage_info <- reactive({
    
    rt_coverage_trend() %>% 
      as.data.frame() %>%
      group_by(rt_date, in_stock) %>% 
      summarise(uniq_product= sum(uniq_product), visit= sum(visit)) %>%
      ungroup() %>% 
      group_by(rt_date) %>% 
      #mutate(perc = visit/sum(visit)) %>%
      mutate(total_uniq_product = sum(uniq_product),
             total_visit = sum(visit),
             perc_visit = visit/sum(visit), 
             perc_uniq_prd = uniq_product/sum(uniq_product) ) 
    
  })
  
  
  cid_coverage <- reactive({
    
    performance_breakdown() %>% 
      mutate(bid_date = as.character(bid_date), cid = as.character(cid), have_event =as.character(have_event)) %>%
      mutate(major_offset_grp = if_else(is.na(major_offset_date), 'NA',
                                        if_else(major_offset_date >= 0 & major_offset_date <= 7, 'main_offset_0_7', 'main_offset_8_28'))) %>%
      mutate(all_offset_grp = if_else(is.na(all_offset_date), 'NA',
                                      if_else(all_offset_date >= 0 & all_offset_date <= 7, 'all_offset_0_7', 'all_offset_8_28')))
    
  })
  ##' output here
  ##' some results are from reactive() function above
  
  output$improve_ways <- renderUI({
    k <- knitr::knit(input = "improve_ways.md", quiet = T)
    HTML(markdown::markdownToHTML(k, fragment.only = T))
  })

  
  output$note <- renderUI({
    k <- knitr::knit(input = "note.md", quiet = T)
    HTML(markdown::markdownToHTML(k, fragment.only = T))
  })
  
  
  output$total_bids <- renderValueBox({
    
    performance_breakdown() %>%
      pull(show) %>% sum() %>%
      as.integer() %>%
      prettyNum(big.mark = ",") %>%
      valueBox(subtitle = "Number of bid requests", color = 'olive')
  })
  
  output$no_event_rate <- renderValueBox({
    performance_breakdown() %>% 
      group_by(have_event) %>% 
      summarise(n = sum(show)) %>% 
      mutate(perc = n/sum(n)) %>%
      filter(have_event == 'no_event') %>% 
      pull(perc) %>%
      round(2) %>%
      valueBox(
        subtitle = "No event rate",
        color = "blue"
      )
  })
  
  output$normal_model_rate <- renderValueBox({
    
    performance_breakdown() %>%
      group_by(is_normal) %>% 
      summarise(total_show = sum(show)) %>% 
      mutate(perc = total_show / sum(total_show)) %>% 
      filter(is_normal == 'normal') %>%
      pull(perc) %>%
      round(2) %>%
      valueBox(
        subtitle = "normal model rate -- compare with Grafana coverage dashboard",
        color = "red"
      )
  })
  
  output$campaign_quality_table <- DT::renderDataTable({
    campaign_quality() %>%
      datatable() %>% 
      formatPercentage(c("atr_ratio","cost_ratio", "cpm_ratio"), 2)
    
    
  })
  
  
  
  output$campaign_quality_plot <- renderPlotly({
    p <- campaign_quality() %>% head(20) %>%
      filter(cost_ratio < 5) %>%
      ggplot(aes(x = cpm_ratio, y = atr_ratio)) + 
      geom_point(aes(size = cost_ratio, color = site_id))
    
    print(ggplotly(p) %>%
            layout(plot_bgcolor='rgba(0, 0, 0, 0)') %>% 
            layout(paper_bgcolor='rgba(0, 0, 0, 0)') )
    
  })
  
  output$offset_8_28 <- renderValueBox({
    offset_info() %>%
      filter(offset_date_grp != '0-7') %>% 
      pull(perc_main_event) %>%
      round(2) %>%
      valueBox(
        subtitle = "Offset -- percentage of 8-28 days in main event",
        color = 'yellow'
      )
  })  
  
  output$offset_0_7 <- renderValueBox({
    offset_info() %>%
      filter(offset_date_grp == '0-7') %>% 
      pull(perc_main_event) %>%
      round(2) %>%
      valueBox(
        subtitle = "Offset -- percentage of 0-7 days in main event",
        color = 'purple'
      )
  })
  
  output$summary_event_offset <- DT::renderDataTable({
    performance_breakdown() %>%  
      mutate(major_offset_grp = if_else(is.na(major_offset_date), 'NA',
                                        if_else(major_offset_date >= 0 & major_offset_date <= 7, 
                                                'main_offset_0_7', 'main_offset_8_28'))) %>%
      mutate(all_offset_grp = if_else(is.na(all_offset_date), 'NA',
                                      if_else(all_offset_date >= 0 & all_offset_date <= 7, 
                                              'all_offset_0_7', 'all_offset_8_28'))) %>%
      group_by(bid_date, is_normal, alg, have_event,major_offset_grp,  all_offset_grp) %>%
      summarise(show= sum(show), click = sum(click), action = sum(action), cost = sum(cost)*1e-7, revenue = sum(revenue)*1e-7) %>%
      group_by(bid_date) %>%
      mutate(coverage = show/sum(show), ctr = click/ show, cvr = action/ click , atr = action/show, 
             CPA = round(cost/action,2), ROAS = revenue/cost) %>%
      datatable() %>%
      formatPercentage(c("coverage","ctr", "cvr", "atr"), 2)
  })
  
  
  
  output$rt_datafeed_coverage <- renderValueBox({
    rt_coverage_data() %>% as.data.frame() %>%
      filter(in_stock == 'in stock') %>%
      pull(visit_perc) %>%
      round(2) %>%
      valueBox(
        subtitle = "Retargeting streaming - datafeed product coverage",
        color = 'aqua'
      )
  })
  
  output$rt_coverage_rate <- renderPlotly({
    g <- rt_coverage_info() %>%
      ggplot(.,aes(x = rt_date, y = perc_visit, group = in_stock, color = in_stock)) + geom_line() + 
      theme(axis.text.x=element_text(angle=35, hjust=1))
    print ( ggplotly(g) %>% 
              layout(plot_bgcolor='rgba(0, 0, 0, 0)') %>% 
              layout(paper_bgcolor='rgba(0, 0, 0, 0)') )
  })
  
  
  output$rt_coverage_rate_table <- DT::renderDataTable({
    rt_coverage_info() %>% datatable() %>%
      formatPercentage(c("perc_visit", "perc_uniq_prd"),2)
  })
  
  
  
  output$overview_plot <- renderPlotly({
    
    res <-  performance_breakdown() %>%  
      mutate(major_offset_grp = dplyr::if_else(is.na(major_offset_date), 'NA',
                                               if_else(major_offset_date >= 0 & major_offset_date <= 7, 'main_offset_0_7', 'main_offset_8_28'))) %>%
      mutate(all_offset_grp = dplyr::if_else(is.na(all_offset_date), 'NA',
                                             if_else(all_offset_date >= 0 & all_offset_date <= 7, 'all_offset_0_7', 'all_offset_8_28'))) %>%
      group_by(bid_date, is_normal, have_event,major_offset_grp) %>%
      summarise(show= sum(show), click = sum(click), action = sum(action), cost = sum(cost)*1e-7, revenue = sum(revenue)*1e-7) %>%
      group_by(bid_date) %>%
      mutate(coverage = show/sum(show), 
             ctr = click/ show, 
             cvr = action/ click , 
             atr = action/show, 
             CPA = round(cost/action,2), ROAS = revenue/cost)  %>% 
      group_by(is_normal) %>% mutate(cum_coverage = sum(coverage)) %>%
      select(is_normal, have_event, major_offset_grp, show)
    
    sunburstDF <- as.sunburstDF(res, valueCol = "show")
    plot_ly(data = sunburstDF, 
            ids = ~ids, 
            labels= ~labels, 
            parents = ~parents, 
            values= ~values, 
            type='sunburst', 
            branchvalues = 'total',     
            textinfo = "label+percent entry") %>%
      layout(plot_bgcolor='rgba(0, 0, 0, 0)') %>% 
      layout(paper_bgcolor='rgba(0, 0, 0, 0)') 
  })
  
  
  
  
  output$all_comparison_plot <- renderPlotly({
    r <-  performance_breakdown_all() %>% 
      mutate(campaign_info = paste(site_id, ';', oid, ';', vertical, ';', name)) %>%
      mutate(offset_date_grp = if_else( major_offset_date <= 7, '0-7', 
                                        if_else( major_offset_date > 7 & major_offset_date < 28, '8-28',
                                                 'others'))) %>%
      filter(vertical %in% c('ec', 'app_reengagement')) %>%
      group_by(site_id, campaign_info, is_normal, have_event, offset_date_grp ) %>% 
      summarise(show = sum(show), click = sum(click), cost = sum(cost)/ 1e7) %>%
      group_by(campaign_info ) %>%
      mutate(coverage = show/sum(show)) %>%
      mutate(model_info = paste(is_normal, ';', have_event, ':', offset_date_grp))  %>%
      group_by(campaign_info) %>%
      mutate(total_show = sum(show), total_cost = sum(cost)) %>%
      group_by(site_id) %>%
      mutate(all_campaign_show = sum(show))
    
    print(
      ggplotly(
        ggplot(r %>% filter(all_campaign_show > 100000 ), 
               aes(x = reorder(campaign_info, all_campaign_show),  
                   y = coverage, 
                   fill = model_info, 
                   text = paste(
                     'campaign_info =', campaign_info,
                     '<br>model_info =', model_info,
                     '<br>coverage =', round(coverage,2)
                     
                   )#,
                   #label = round(coverage,2)
               )) +
          geom_bar(stat = 'identity',
                   position = position_stack(reverse = TRUE),
                   width = 0.8
          ) + 
          geom_text( aes(label = round(total_cost), group = campaign_info), stat = 'summary', fun = sum, vjust = -1, size = 2) + 
          ggtitle('Multiple sites comparison overview plot by total show') + coord_flip() +
          # facet_grid(rows = vars(grp), space = "free_y", scales = "free_y") +
          xlab("") , height =  1000, width = 1200, tooltip = "text")  %>%
        layout(plot_bgcolor='rgba(0, 0, 0, 0)') %>%
        layout(paper_bgcolor='rgba(0, 0, 0, 0)')
      
    )
    
    
    
    
  })
  
  
  output$offset_plot <- renderPlotly({
    g <- performance_breakdown() %>%
      group_by(major_offset_date) %>% 
      summarise(show = sum(show)) %>%
      mutate(perc = show/sum(show)) %>% 
      mutate(cum_perc_main_event = cumsum(perc)) %>%
      left_join( performance_breakdown() %>% 
                   group_by(all_offset_date) %>% 
                   summarise(show = sum(show)) %>% 
                   mutate(perc = show/sum(show)) %>% 
                   mutate(cum_perc_all_event = cumsum(perc)),
                 by = c('major_offset_date' = 'all_offset_date')) %>%
      ggplot(., aes(x = major_offset_date)) + geom_line(aes(y = cum_perc_main_event, color = 'main_event')) +
      geom_line(aes(y = cum_perc_all_event, color = 'all_event')) + ggtitle('main event vs. all event offset plots') + xlim(c(0,30)) 
    
    print(
      ggplotly(
        g
      ) %>% 
        layout(plot_bgcolor='rgba(0, 0, 0, 0)') %>% 
        layout(paper_bgcolor='rgba(0, 0, 0, 0)') 
    )
  })
  
  
  
  output$coverage_by_cid <- renderPlotly({
    r <- cid_coverage() %>%
      group_by(bid_date, cid, is_normal, have_event, major_offset_grp) %>%
      summarise(show= sum(show), click = sum(click), action = sum(action), cost = sum(cost)*1e-7, revenue = sum(revenue)*1e-7) %>%
      group_by(bid_date, cid) %>%
      mutate(coverage = show/sum(show), ctr = click/ show, cvr = action/ click , atr = action/show, 
             CPA = round(cost/action,2), ROAS = revenue/cost)  %>%
      mutate(model_info = paste(is_normal, ';', have_event,';', major_offset_grp)) %>%
      group_by(cid) %>%
      mutate(total_show = sum(show), total_cost = sum(cost))
    
    g <-  ggplot(r, 
                 aes(x = reorder(cid, -total_cost), y = coverage, fill = model_info, 
                     text = paste(
                       '<br>model_info =', model_info,
                       '<br>coverage =', round(coverage,2)))) + 
      geom_bar(
        stat="identity",
        position= position_stack(reverse = TRUE)) + 
      geom_text( aes(label = round(total_cost), group = cid), stat = 'summary', fun = sum, vjust = -1, size = 3) + 
      theme(axis.text.x = element_text(angle = 45))
    
    
    print(
      ggplotly(g, tooltip = "text") %>% 
        layout(plot_bgcolor='rgba(0, 0, 0, 0)') %>% 
        layout(paper_bgcolor='rgba(0, 0, 0, 0)') 
    )
  })
  
  
  output$coverage_by_cid_performance_table <- DT::renderDataTable({
    cid_coverage() %>%
    group_by(bid_date, cid, is_normal, alg, have_event,major_offset_grp,  all_offset_grp) %>%
      summarise(show= sum(show), click = sum(click), action = sum(action), cost = sum(cost)*1e-7, revenue = sum(revenue)*1e-7) %>%
      mutate( ctr = click/ show, cvr = action/ click , atr = action/show,  cpm = cost/ (show/1000),
              CPA = round(cost/action,2), ROAS = revenue/cost) %>%
      group_by(bid_date, cid) %>%
      mutate(coverage = show/sum(show)) %>%
      arrange(desc(coverage)) %>%
      datatable() %>% 
      formatPercentage(c("coverage", "ctr", "cvr", "atr", "cpm"),2) %>%
      formatRound(c("cost", "revenue", "ROAS"), 2)
  })
  
  
  
  observeEvent(input$column_clicked != "", {
    if (input$month == "99") {
      updateSelectInput(session, "month", selected = input$column_clicked)
    } else {
      day <- input$column_clicked
      month <- input$month
      tab_title <- paste(
        input$airline, "-", month.name[as.integer(month)], "-", day
      )
      if (!(tab_title %in% tab_list)) {
        appendTab(
          inputId = "tabs",
          tabPanel(
            tab_title,
            DT::renderDataTable(
              get_details(day = day)
            )
          )
        )
        tab_list <<- c(tab_list, tab_title)
      }
      updateTabsetPanel(session, "tabs", selected = tab_title)
    }
  },
  ignoreInit = TRUE
  )
  
  
}